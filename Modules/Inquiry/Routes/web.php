<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth','web'],'prefix' => 'dashboard/inquiry','as'=>'inquiry.'], function()
{	
    Route::get('/create', 	 		['uses'=>'InquiryController@create',		'as'=>'create']);
    Route::post('/store',  	 		['uses'=>'InquiryController@store',			'as'=>'store']);

    Route::get('/view',  			['uses'=>'InquiryController@view',			'as'=>'view']);

    Route::get('/show/{id}', 	    ['uses'=>'InquiryController@show',			'as'=>'show']);

    Route::get('/edit/{id}', 	    ['uses'=>'InquiryController@edit',			'as'=>'edit']);

    Route::post('/update/{id}',  	['uses'=>'InquiryController@update',		'as'=>'update']);

    Route::get('/delete/{id}',  	['uses'=>'InquiryController@delete',		'as'=>'delete']);
});
