@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-6">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
            </div>
            <div class="col-md-3">
              <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('category.create')}}">Add Category</a></li>
            </div>
            <div class="col-md-3">
              <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('inquiry.view')}}">View Company</a></li>
            </div>

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    {{-- form started --}}
    <form method="post" action="{{Route('inquiry.store')}}" enctype="multipart/form-data" onsubmit="return checkForm(this);">
      @csrf

      <div class="row">
        <!-- Slide -->
        <div class="col-lg-12">
          <!-- Slide Content -->
          <div class="form-wrapper well">
           <!-- category -->
           <div class="form-group">
            <div class="row">
              <div class="col-sm-3">
                <label class="control-label">Category:</label>
              </div>

              <div class="col-sm-9">
               <select name="category" class="form-control" value="{{old('category')}}">
                @foreach($data['category'] as $category)
                <option>  {{$category->category}}</option>
                @endforeach
              </select>
              @if($errors->has('category'))
              <span class="text-danger">
                *{{$errors->first('category')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>
        <!-- end of category -->

        <!-- company -->
        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Company:</label>
            </div>

            <div class="col-sm-9">
              <input type="text" class="form-control"  placeholder="Example: kitenepal" name="company" value="{{old('company')}}">
              @if($errors->has('company'))
              <span class="text-danger">
                *{{$errors->first('company')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>
        <!-- end of company -->

        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Address:</label>
            </div>

            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Example: Bhaktapur" name="address" value="{{old('address')}}">
              @if($errors->has('address'))
              <span class="text-danger">
                *{{$errors->first('address')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Email:</label>
            </div>
            <div class="col-sm-9">
              <input type="email" class="form-control"  placeholder="example:info@kitenepal.com" name="email" value="{{old('email')}}">
              @if($errors->has('email'))
              <span class="text-danger">
                *{{$errors->first('email')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <!-- Caption -->
        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Phone Number:</label>
            </div>
            <div class="col-sm-9">
              <input type="number" class="form-control" placeholder="Example: 01-46#####" name="phoneno" value="{{old('phoneno')}}">
              @if($errors->has('phoneno'))
              <span class="text-danger">
                *{{$errors->first('phoneno')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Contact Person:</label>
            </div>
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Example: Bill Gates" name="contactperson" value="{{old('contactperson')}}">
              @if($errors->has('contactperson'))
              <span class="text-danger">
                *{{$errors->first('contactperson')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-3">
              <label class="control-label">Mobile Number:</label>
            </div>
            <div class="col-sm-9">
              <input type="number" class="form-control" placeholder="Example:9841######" name="mobileno" value="{{old('mobileno')}}">
              @if($errors->has('mobileno'))
              <span class="text-danger">
                *{{$errors->first('mobileno')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <button type="submit" name="myButton" class="btn btn-primary"><b>Save</b></button>
      </div>
    </div>
    <br>
  </div>
</form>
{{-- form end --}}

</div>
</section>
@endsection
