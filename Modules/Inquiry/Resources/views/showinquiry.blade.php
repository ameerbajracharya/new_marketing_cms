@extends('dashboard::layouts.master')
@section('title')


{{$_panel}} :: Show


@endsection
@section('content')
<style>
  label{
    font-family: sans-serif;
    font-weight: 600;
    letter-spacing: 1px;
    margin: 2px;

  }
</style>
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
   <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-8">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('inquiry.create')}}">Add</a></li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('inquiry.view')}}">View</a></li>
                        </div>

                    </div>
                </ol>
      </div>
    </div>

    <div class="row">

     <div class="col-md-5">

      <div class="form-wrapper well">

        <div class="form-group">
          <label class=" control-label">Category:  {{$data['inquiry']->category}}</label>
          <br>
        </div>

        <div class="form-group">
          <label class=" control-label">Company:  {{$data['inquiry']->company}}</label>
          <br>
        </div>


        <div class="form-group">
         <label class=" control-label">Address:  {{$data['inquiry']->address}}</label>
         <br>
       </div>


       <div class="form-group">
        <label class=" control-label">Email:  {{$data['inquiry']->email}}</label>
        <br>
      </div>
      <!-- Caption -->

      <div class="form-group">
        <label class=" control-label">Contact Person:  {{$data['inquiry']->contactperson}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class=" control-label">Phone Number:  {{$data['inquiry']->phoneno}}</label>
        <br>
      </div>

    </div>
  </div>

  <div class="col-md-7">

    <!-- Slide -->

    <!-- Slide Content -->

    <div class="form-wrapper well">

      <div class="form-group">
        <label class=" control-label">Mobile Number:  {{$data['inquiry']->mobileno}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class=" control-label">Website:  {{$data['inquiry']->website}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class="control-label">Software: {{$data['inquiry']->software}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class="control-label">Remarks:  {{$data['inquiry']->remarks}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class=" control-label">Facebook Page:  {{$data['inquiry']->facebook}}</label>
        <br>
      </div>

      <div class="form-group">
        <label class="control-label">Image:</label>
        @if($data['inquiry']->$_databaseimage)
        <img src="{{url($_folderpath.DIRECTORY_SEPARATOR.$data['inquiry']->$_databaseimage)}}" height="120px" width="200px">
        @endif
        <br>
      </div>

    </div>
  </div>
</div>

</div>
</section>
@endsection
