@extends('dashboard::layouts.master')
@section('title')


{{$_panel}} :: Edit


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-8">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
            </div>
            <div class="col-md-2">
              <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('inquiry.create')}}">Add</a></li>
            </div>
            <div class="col-md-2">
              <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('inquiry.view')}}">View</a></li>
            </div>

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    {{-- form started --}}
    <form method="post" action="{{Route('inquiry.update',$data['inquiry']->id)}}" enctype="multipart/form-data" onsubmit="return checkForm(this);">
      @csrf

      <div class="row">
        <!-- Slide -->
        <div class="col-md-6">
          <!-- Slide Content -->
          <div class="form-wrapper well">

            <div class="form-group">
              <label class="col-sm-4 control-label">Category:</label>
              <select name="category" class="form-control" value="{{old('category')}}" style="width:50%;">
                @foreach($data['category'] as $category)
                <option>  {{$category->category}}</option>
                @endforeach
              </select>
              @if($errors->has('category'))
              <span class="text-danger">
                *{{$errors->first('category')}}
              </span>
              @endif
              <br>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Company:</label>
              <input type="text" class="form-control"  placeholder="Enter Company's name" name="company" value="{{$data['inquiry']->company}}" style="width:50%;">
              @if($errors->has('company'))
              <span class="text-danger">
                *{{$errors->first('company')}}
              </span>
              @endif
              <br>
            </div>


            <div class="form-group">
             <label class="col-sm-4 control-label">Address:</label>
             <input type="text" class="form-control"  placeholder="Address" name="address" value="{{$data['inquiry']->address}}" style="width:50%;">
             @if($errors->has('address'))
             <span class="text-danger">
              *{{$errors->first('address')}}
            </span>
            @endif
            <br>
          </div>


          <div class="form-group">
            <label class="col-sm-4 control-label">Email:</label>
            <input type="email" class="form-control" placeholder="Email" name="email" value="{{$data['inquiry']->email}}" style="width:50%;">
            @if($errors->has('email'))
            <span class="text-danger">
              *{{$errors->first('email')}}
            </span>
            @endif
            <br>
          </div>
          <!-- Caption -->

          <div class="form-group">
            <label class="col-sm-4 control-label">Phone Number:</label>
            <input type="text" class="form-control" placeholder="Phone Number" name="phoneno" value="{{$data['inquiry']->phoneno}}" style="width:50%;">
            @if($errors->has('phoneno'))
            <span class="text-danger">
              *{{$errors->first('phoneno')}}
            </span>
            @endif
            <br>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Contact Person:</label>
            <input type="text" class="form-control" placeholder="Contact Person's Name" name="contactperson" value="{{$data['inquiry']->contactperson}}" style="width:50%;">
            @if($errors->has('contactperson'))
            <span class="text-danger">
              *{{$errors->first('contactperson')}}
            </span>
            @endif
            <br>
          </div>



        </div>
      </div>

      <div class="col-md-6">
        <!-- Slide Content -->
        <div class="form-wrapper well">
          <div class="form-group">
            <label class="col-sm-3 control-label">Mobile:</label>
            <input type="text" class="form-control" placeholder="Mobile Number" name="mobileno" value="{{$data['inquiry']->mobileno}}" style="width:50%;">
            @if($errors->has('mobileno'))
            <span class="text-danger">
              *{{$errors->first('mobileno')}}
            </span>
            @endif
            <br>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Website:</label>
            <input type="url" class="form-control" placeholder="Link" name="website" value="{{$data['inquiry']->website}}" style="width:50%;">
            @if($errors->has('website'))
            <span class="text-danger">
              *{{$errors->first('website')}}
            </span>
            @endif
            <br>
          </div>





          <div class="form-group">
            <label class="col-sm-3 control-label">Software:</label>
            <input type="radio" name="software" value="{{$data['inquiry']->software}}" >Yes
            <br>
            <input type="radio"   name="software" value="{{$data['inquiry']->software}}";>No
            @if($errors->has('software'))
            <span class="text-danger">
              *{{$errors->first('software')}}
            </span>
            @endif
            <br>
          </div>


          <div class="form-group">
            <label class="col-sm-3 control-label">Remarks:</label>
            <input type="text" class="form-control" placeholder="Why you select Yes" name="remarks" value="{{$data['inquiry']->remarks}}" style="width:50%;">
            <br>
          </div>



          <div class="form-group">
            <label class="col-sm-3 control-label">Facebook:</label>
            <input type="url" class="form-control" placeholder="Link" name="facebook" value="{{$data['inquiry']->facebook}}" style="width:50%;">
            @if($errors->has('facebook'))
            <span class="text-danger">
              *{{$errors->first('facebook')}}
            </span>
            @endif
            <br>
          </div>  


          <div class="form-group">
            <label class="col-sm-3 control-label">UploadImage:  </label>
            <input type="file" class="form-control"  name="mainimage" value="{{$data['inquiry']->mainimage}}" style="width:50%;">
            <br>
            @if($data['inquiry']->$_databaseimage))
            <img src="{{url($_folderpath.DIRECTORY_SEPARATOR.$data['inquiry']->$_databaseimage)}}" height="120px" width="200px">
            @else
            <p class="text-center">No Image Stored.</p>
            @endif
            @if($errors->has('mainimage'))
            <span class="text-danger">
              *{{$errors->first('mainimage')}}
            </span>
            @endif
          </div>
        </div>
      </div>
    </div> 
    <br> 
    <button type="submit" name="myButton" class="btn btn-primary"><b>Update</b></button>
  </form>
  {{-- form end --}}
  
</div>
</section>
@endsection
