@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: List


@endsection

@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-8">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}} :: List</li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('inquiry.create')}}">Company Create</a></li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><a href="{{Route('followup.show')}}"><i class="fa fa-eye"></i>Show Follow Ups</a></li>
                        </div>
                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of user table -->
        <div class="row">
            <div class="col-lg-12">
                <!--left body: usertable -->
                <div class="table-responsive">
                    <table class="table">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Category</th>
                                <th>Company</th>
                                <th>Address</th>
                                <th>Phone Number</th>
                                <th>Created</th>
                                <th colspan="3" style="text-align: center;">Action</th>

                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                           @foreach ($data['inquiry'] as $inquiry)
                           <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$inquiry->category}}</td>
                            <td>{{$inquiry->company}}</td>
                            <td>{{$inquiry->address}}</td>
                            <td>{{$inquiry->phoneno}}</td>
                            <td>{{ Carbon\Carbon::parse($inquiry->created_at)->format('Y-M-d')}}</td>
                            <td>
                                <a type="button" class="btn btn-xs btn-primary" href="{{Route('inquiry.show',$inquiry->id)}}">View</a>
                            </td>
                            <td>
                             <a type="button" class="btn btn-xs btn-success" href="{{Route('inquiry.edit',$inquiry->id)}}">Edit</a>
                         </td>
                         @can('isAdmin')
                         <td>
                             <a id="delete" class="btn btn-xs btn-danger" href="{{Route('inquiry.delete',$inquiry->id)}}">Delete</a>
                         </td>
                         @endcan
                     </tr>

                     @endforeach

                 </tbody>
                 <!-- end of table body -->
             </table>
         </div>
             <!-- bottom pagination -->
            <center>
                <div class="col-lg-12">
                   {!! $data['inquiry']->render() !!}
               </div>
           </center>
           <!-- end of bottom pagination -->
       <!-- leftbody : usertable end -->
   </div>
</div>

<!-- bottom pagination -->

<!-- end of bottom pagination -->

</div>
</section>


@endsection
