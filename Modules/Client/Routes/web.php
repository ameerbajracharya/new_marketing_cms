<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth','web'],'prefix' => 'dashboard/client'], function()
{
	Route::get('/', 			['as' => 'client', 			'uses' => 'ClientController@view']);
	Route::get('/create', 		['as' => 'client.create', 	'uses' => 'ClientController@create']);
	Route::post('/store',	 	['as' => 'client.store',		'uses' => 'ClientController@store']);
	Route::get('/edit/{id}',    ['as' => 'client.edit', 		'uses' => 'ClientController@edit']);
	Route::get('/show/{id}',    ['as' => 'client.show', 		'uses' => 'ClientController@show']);
	Route::post('/update/{id}', ['as' => 'client.update',		'uses' => 'ClientController@update']);
	Route::get('/delete/{id}',  ['as' => 'client.delete', 	'uses' => 'ClientController@delete']);
	Route::get('Client_type/{id}',   ['as' => 'client.Client_type',  'uses' => 'ClientController@Clienttype'] );
	Route::get('/status/{id}',  ['as' => 'client.status', 'uses' => 'ClientController@status'] );

});
