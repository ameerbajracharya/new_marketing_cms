@extends('dashboard::layouts.master')

@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}} </li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{route('client.create')}}">Add</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of client table -->
        <div class="row">
            <div class="col-xs-12">
                <!--left body: clienttable -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Contact</th>
                               
                                <th>Status</th>
                                <th>Created</th>
                                <th colspan="3" style="text-align: center;">Setting</th>
                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                          @foreach($data['client'] as $client)
                          <tr>
                              <td>{{$client->name}}</td>
                              <td>{{$client->email}}</td>
                              <td>{{$client->address}}</td>
                              <td>{{$client->contact}}</td>
                              
                                    @if($client->status == 0)
                                        <td>
                                            @if($client->status == 0)
                                                <a href="{{route('client.status',$client->id)}}" class="btn btn-xs  btn-danger">Inactive</a>
                                            @else
                                                <a href="" class="btn btn-xs btn-info">Active</a>
                                            @endif
                                        </td>
                                    @else
                                        <td>
                                            @if($client->status == 1)
                                            <a href="{{route('client.status',$client->id)}}" class="btn btn-xs  btn-info">Active</a>

                                            @else
                                            <a href="" class="btn btn-xs btn-danger">Inactive</a>
                                            @endif
                                        </td>
                                    @endif
                            <td>
                                @if(!$client->created_at == NULL)
                                {{$client->created_at->format('M-d-Y')}}
                                @else
                                <?php echo 'Null'; ?>
                                @endif
                            </td>
                             <td><a href="{{Route('client.show',$client->id)}}" class="btn btn-xs btn-success">Show</a>
                            </td>
                            <td><a href="{{Route('client.edit',$client->id)}}" class="btn btn-xs btn-info">Edit</a>
                            </td>
                            <td><a id="delete" href="{{Route('client.delete',$client->id)}}" class="btn btn-xs btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!-- end of table body -->
                </table>
            </div>
            <!-- bottom pagination -->
                <div class="col-lg-4">
                    <center>
                            {!! $data['client']->render() !!}
                    </center>
                </div>
            <!-- end of bottom pagination -->
            <!-- leftbody : clienttable end -->
        </div>
    </div>

</div>
</section>


@endsection
