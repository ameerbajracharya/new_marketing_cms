@extends('dashboard::layouts.master')
@section('title')


{{$_panel}} :: Edit


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
      <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-8">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('client.create')}}">Add</a></li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('client')}}">View</a></li>
                        </div>

                    </div>
                </ol>>
      

      <!-- End of Title Bar -->
    </div>

    
  </div>
  {{-- overviewend --}}

  {{-- form started --}}
  <form method="post" action="{{Route('client.update',$data['client']->id)}}" enctype="multipart/form-data" onsubmit="return checkForm(this);>
    @csrf
    <div class="row">
        

          <div class="col-lg-6">
            <div class="form-wrapper well">
              <div class="form-group">

              <!-- name -->
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Name:</label>
                </div>

                <div class="col-sm-8">
                   <input type="text" class="form-control"  placeholder="Enter Company's name" name="name" value="{{$data['client']->name}}" style="width:50%;">
                    @if($errors->has('name'))
                    <span class="text-danger">
                      *{{$errors->first('name')}}
                    </span>
                    @endif
                    <br>
                </div>

              </div>
              <!-- end of name -->
              <br>
            </div> 

            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Address:</label>
                </div>

                <div class="col-sm-8">
                    <input type="text" class="form-control"  placeholder="Address" name="address" value="{{$data['client']->address}}" style="width:50%;">
                   @if($errors->has('address'))
                   <span class="text-danger">
                    *{{$errors->first('address')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Email:</label>
                </div>
                <div class="col-sm-8">
                  <input type="email" class="form-control" placeholder="Email" name="email" value="{{$data['client']->email}}" style="width:50%;">
                    @if($errors->has('email'))
                    <span class="text-danger">
                      *{{$errors->first('email')}}
                    </span>
                    @endif
                    <br>
                </div>
              </div>
              <br>
            </div>

            <!-- Caption -->
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Phone Number:</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Phone Number" name="contact" value="{{$data['client']->contact}}">
                  @if($errors->has('contact'))
                  <span class="text-danger">
                    *{{$errors->first('contact')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Category:</label>
                </div>
                <div class="col-sm-8">
                   <select name="category" value="" >  
                      <option>  Pick a Category</option>
                      @foreach($data['category'] as $category)
                      <option>  {{$category->category}}</option>
                      @endforeach
                  </select>
                  @if($errors->has('category'))
                  <span class="text-danger">
                    *{{$errors->first('category')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            </div>
          </div>
          <div class="col-lg-6">
             <div class="form-wrapper well">
              <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Contact Person:</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Contact Person's name" name="contactperson" value="{{$data['client']->contactperson}}">
                  @if($errors->has('contactperson'))
                  <span class="text-danger">
                    *{{$errors->first('contactperson')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Mobile Number:</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Contact Person's number" name="mobileno" value="{{$data['client']->mobileno}}">
                  @if($errors->has('mobileno'))
                  <span class="text-danger">
                    *{{$errors->first('mobileno')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            
            <div class="form-group">
              <div class="row">
                 <label class="col-lg-4 control-label">Contract Date:</label>
                 <input type="date" class="form-control col-lg-8" placeholder="2018-07-22" name="contractdate" value="{{$data['client']->contractdate}}" style="width:50%;">
                 @if($errors->has('contractdate'))
                 <span class="text-danger">
                  *{{$errors->first('contractdate')}}
                </span>
                @endif
                <br>
              </div>
              <br>

            </div>
             <div class="form-group">
              <div class="row">
                 <label class="col-lg-4 control-label">Exp Date:</label>
                 <input type="date" class="form-control col-lg-8 " placeholder="2018-07-22" name="expdate" value="{{$data['client']->expdate}}" style="width:50%;">
                 @if($errors->has('expdate'))
                 <span class="text-danger">
                  *{{$errors->first('expdate')}}
                </span>
                @endif
                <br>
              </div>
            </div>
            <br>
            
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">About:</label>
                </div>
                <div class="col-sm-8">
                  <textarea class="form-control"  name="about" >{{$data['client']->About}}</textarea>
              
                  @if($errors->has('about'))
                  <span class="text-danger">
                    *{{$errors->first('about')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
             </div>
            
          </div>
        </div>
    

    <button type="submit" name="myButton" class="btn btn-primary"><b>Update</b></button>
  </form>
  {{-- form end --}}
  

</div>
</section>
@endsection
