@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-6">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
            </div>
            <div class="col-md-2">
              <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('client')}}">List</a></li>
            </div>
            <div class="col-md-2">
              <li class="text-right"><i class="fa fa-edit"></i><a href="{{Route('client.edit',$data['client']->id)}}">Edit</a></li>
            </div>
            <div class="col-md-2">
              <li class="text-right"><i class="fa fa-trash-o"></i><a href="{{Route('client.delete',$data['client']->id)}}">Delete</a></li>
            </div>

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    <form>
      <div class="row">
        

        <div class="col-lg-6">
          <div class="form-wrapper well">
            <div class="form-group">

              <!-- name -->
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Name:</label>
                </div>

                <div class="col-sm-8">
                 <input type="text" class="form-control"  placeholder="Enter Company's name" name="name" value="{{$data['client']->name}}" style="width:50%;">
                 <br>
               </div>

             </div>
             <!-- end of name -->
             <br>
           </div> 

           <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Address:</label>
              </div>

              <div class="col-sm-8">
                <input type="text" class="form-control"  placeholder="Address" name="address" value="{{$data['client']->address}}" style="width:50%;">
                
              </div>
            </div>
            <br>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Email:</label>
              </div>
              <div class="col-sm-8">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{$data['client']->email}}" style="width:50%;">
                
                <br>
              </div>
            </div>
            <br>
          </div>

          <!-- Caption -->
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Phone Number:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Phone Number" name="contact" value="{{$data['client']->contact}}">
                
              </div>
            </div>
            <br>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Category:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Category" name="category" value="{{$data['client']->category}}">
                
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
       <div class="form-wrapper well">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4">
              <label class="control-label">Contact Person:</label>
            </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" placeholder="Contact Person's name" name="contactperson" value="{{$data['client']->contactperson}}">
              
            </div>
          </div>
          <br>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-sm-4">
              <label class="control-label">Mobile Number:</label>
            </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" placeholder="Contact Person's number" name="mobileno" value="{{$data['client']->mobileno}}">
              
            </div>
          </div>
          <br>
        </div>
        
        <div class="form-group">
          <div class="row">
           <label class="col-lg-4 control-label">Contract Date:</label>
           <input type="date" class="form-control col-lg-8" placeholder="2018-07-22" name="contractdate" value="{{$data['client']->contractdate}}" style="width:50%;">
           
           <br>
         </div>
         <br>

       </div>
       <div class="form-group">
        <div class="row">
         <label class="col-lg-4 control-label">Exp Date:</label>
         <input type="date" class="form-control col-lg-8 " placeholder="2018-07-22" name="expdate" value="{{$data['client']->expdate}}" style="width:50%;">
         
         <br>
       </div>
     </div>
     <br>
     
     <div class="form-group">
      <div class="row">
        <div class="col-sm-4">
          <label class="control-label">About:</label>
        </div>
        <div class="col-sm-8">
          <textarea class="form-control"  name="about" >{{$data['client']->About}}</textarea>
          
        </div>
      </div>
      <br>
    </div>
  </div>
  
</div>
</div>

</form>
{{-- form end --}}
</div>
</section>
@endsection