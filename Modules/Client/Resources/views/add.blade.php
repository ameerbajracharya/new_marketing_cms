@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-6">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
            </div>
            <div class="col-md-3">
              <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('category.create')}}">Add Task</a></li>
            </div>
            {{--            <div class="col-md-3">--}}
              {{--              <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('task')}}">View</a></li>--}}
            {{--            </div>--}}

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    {{-- form started --}}
    <form method="post" action="{{Route('client.store')}}" enctype="multipart/form-data" onsubmit="return checkForm(this);">
      @csrf
      <div class="row">


        <div class="col-lg-6">
          <div class="form-wrapper well">
            <div class="form-group">

              <!-- name -->
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Name:</label>
                </div>

                <div class="col-sm-8">
                  <input type="text" class="form-control"  placeholder="Enter Company's name" name="name" value="{{old('name')}}">
                  @if($errors->has('name'))
                  <span class="text-danger">
                    *{{$errors->first('name')}}
                  </span>
                  @endif
                </div>

              </div>
              <!-- end of name -->
              <br>
            </div> 

            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Address:</label>
                </div>

                <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Address" name="address" value="{{old('address')}}">
                  @if($errors->has('address'))
                  <span class="text-danger">
                    *{{$errors->first('address')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Email:</label>
                </div>
                <div class="col-sm-8">
                  <input type="email" class="form-control"  placeholder="Email" name="email" value="{{old('email')}}">
                  @if($errors->has('email'))
                  <span class="text-danger">
                    *{{$errors->first('email')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>

            <!-- Caption -->
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Phone Number:</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Phone Number" name="contact" value="{{old('contact')}}">
                  @if($errors->has('contact'))
                  <span class="text-danger">
                    *{{$errors->first('contact')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label class="control-label">Category:</label>
                </div>
                <div class="col-sm-8">
                  <select name="category" class="form-control" value="{{old('category')}}">  
                    <option>  Pick a Category</option>
                    @foreach($data['category'] as $category)
                    <option>  {{$category->category}}</option>
                    @endforeach
                  </select>
                  @if($errors->has('category'))
                  <span class="text-danger">
                    *{{$errors->first('category')}}
                  </span>
                  @endif
                </div>
              </div>
              <br>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
         <div class="form-wrapper well">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Contact Person:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Contact Person's name" name="contactperson" value="{{old('contactperson')}}">
                @if($errors->has('contactperson'))
                <span class="text-danger">
                  *{{$errors->first('contactperson')}}
                </span>
                @endif
              </div>
            </div>
            <br>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-4">
                <label class="control-label">Mobile Number:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="Contact Person's number" name="mobileno" value="{{old('mobileno')}}">
                @if($errors->has('mobileno'))
                <span class="text-danger">
                  *{{$errors->first('mobileno')}}
                </span>
                @endif
              </div>
            </div>
            <br>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-sm-4" >
               <label class="control-label">Contract Date:</label>
             </div>
             <div class="col-sm-8">
               <input type="date" class="form-control" placeholder="2018-07-22" name="contractdate" value="{{old('contractdate')}}">
               @if($errors->has('contractdate'))
               <span class="text-danger">
                *{{$errors->first('contractdate')}}
              </span>
              @endif
            </div>
          </div>
          <br>
        </div>

        <div class="form-group">
          <div class="row">
           <div class="col-sm-4" >
             <label class="control-label">Exp Date:</label>
           </div>
           <div class="col-sm-8">
             <input type="date" class="form-control" placeholder="2018-07-22" name="expdate" value="{{old('expdate')}}">
             @if($errors->has('expdate'))
             <span class="text-danger">
              *{{$errors->first('expdate')}}
            </span>
            @endif
          </div>
        </div>
        <br>
      </div>


      <div class="form-group">
        <div class="row">
          <div class="col-sm-4">
            <label class="control-label">About:</label>
          </div>
          <div class="col-sm-8">
            <textarea class="form-control"  name="about" >{{old('contactperson')}}</textarea>

            @if($errors->has('about'))
            <span class="text-danger">
              *{{$errors->first('about')}}
            </span>
            @endif
          </div>
        </div>
        <br>
      </div>
    </div>

  </div>
</div>
<button type="submit" name="myButton" class="btn btn-primary"><b>Save</b></button>
</form>
{{-- form end --}}


</div>
</section>
@endsection
