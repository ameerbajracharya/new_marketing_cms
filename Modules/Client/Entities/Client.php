<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entites\Category;

class Client extends Model
{
	protected $fillable = ['name','email','address','contact','contactperson','mobileno','about','contractdate','expdate','category'];

	public function category()
	{
		return $this->hasMany(Category::class);
	}
}
