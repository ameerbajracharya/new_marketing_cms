<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entites\Client;
use Modules\Inquiry\Entites\Inquiry;

class Category extends Model
{
	protected $fillable = ['category'];


	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function inquiries()
	{
		return $this->hasMany(Inquiry::class);
	}
}
