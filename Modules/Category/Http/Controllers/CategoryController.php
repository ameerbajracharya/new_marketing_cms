<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Http\Controllers\CategoryController;
use Modules\Category\Entities\Category;
use App\Http\Controllers\BaseController;
use Session;

class CategoryController extends BaseController

{
    protected $view_path = 'category::';
    protected $base_route = 'category';
    protected $databaseimage = '';
    protected $folder = '';
    protected $panel = 'Category';
    protected $folder_path;
    
    public function __construct(Category $model){
        $this->model = $model;
        // $this->folderpath = public_path('images'. DIRECTORY_SEPARATOR).$this->folder;
    }
    public function view()
    {
        $data['category']= Category::paginate(10);
        return view(parent::commondata($this->view_path.'index'), compact('data'));
    }
    public function create()
    {
        return view(parent::commondata($this->view_path.'add'));
    }

    public function store(Request $request)
    {
        // $this->storeimage($request);
        $data['category']= Category::create($request->all());
        Session::flash('success','category Stored Successfully');
        return redirect()->route('category');
    }


    public function edit($id)
    {
        $data['category'] = Category::find($id);
        return view(parent::commondata($this->view_path.'edit'),compact('data'));
    }
    public function show($id)
    {
        $data['category'] = Category::find($id);
        return view(parent::commondata($this->view_path.'show'),compact('data'));
    }

    public function update(Request$request, $id)
    {
        $data = Category::find($id);
        $data->update($request->all());
        Session::flash('success','category Updated Successfully.');
        return redirect()->Route($this->base_route);    }

    public function delete($id)
    {
      $data = Category::find($id);
      $data->delete();
      Session::flash('success','categoryDeleted Successfully.');
      return redirect()->Route($this->base_route); 
         }

     public function status($id){
        $this->statuschange($id);

        return redirect()->route($this->base_route);
    }
}
