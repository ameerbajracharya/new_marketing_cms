@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
          <div class="card">
                        <div class="card-header">


                            <h3 class="card-title">{{$_panel}} Data</h3>
                            <div class="row">
                                <button class="btn btn-default btn-sm"><a href="{{Route('category')}}"  style="color: #e20909;">
                                        <i class="fa fa-list"></i>
                                        List</a>
                                </button>

                            </div>
                            <div class="card-tools">


                                        <div class="input-group input-group-sm" style="width: 150px;">

                                            <input type="text" name="table_search" class="form-control float-right"
                                                   placeholder="Search">

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-default"><i
                                                        class="fa fa-search"></i></button>
                                            </div>


                                </div>

                            </div>
                        </div>

                    </div>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    {{-- form started --}}
    <form method="post" action="{{Route('category.store')}}" enctype="multipart/form-data" onsubmit="return checkForm(this);">
      @csrf
      <div class="row">
        <div class="form-wrapper well">
          <div class="form-group">
           <!-- name -->
           <div class="row">
            <div class="col-sm-4">
              <label class="control-label">Category:</label>
            </div>

            <div class="col-sm-8">
              <input type="text" class="form-control"  placeholder="Enter Company's name" name="category" value="{{$data['category']->category}}" required>
              @if($errors->has('category'))
              <span class="text-danger">
                *{{$errors->first('category')}}
              </span>
              @endif
            </div>

          </div>
          <!-- end of name -->
          <br>
        </div> 
        <button type="submit" name="myButton"  class="btn btn-primary"><b>Save</b></button>
      </div>
      
    </div>
  </form>
  {{-- form end --}}

</div>
</section>
@endsection
