@extends('dashboard::layouts.master')

@section('content')
@include('dashboard::include.header')
<section class="content">
    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">

                <h3 class="card-title">{{$_panel}} Data</h3>
                <button class="btn btn-default btn-sm"><a href="{{Route('category.create')}}"  style="color: #e20909;">
                    <i class="fa fa-plus"></i>
                Create</a>
            </button>
        </div>

        <!-- end of overstart -->

        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <!--start heading of the table  -->
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Setting</th>
                    </tr>
                </thead>
                <!-- end of table heading -->
                <!-- table body start -->
                <tbody>
                  @foreach($data['category'] as $category)
                  <tr>
                      <td>{{$category->category}}</td>


                      @if($category->status == 0)
                      <td>
                        @if($category->status == 0)
                        <a href="{{route('category.status',$category->id)}}" class="btn btn-xs  btn-danger">Inactive</a>
                        @else
                        <a href="" class="btn btn-xs btn-info">Active</a>
                        @endif
                    </td>
                    @else
                    <td>
                        @if($category->status == 1)
                        <a href="{{route('category.status',$category->id)}}" class="btn btn-xs  btn-info">Active</a>

                        @else
                        <a href="" class="btn btn-xs btn-danger">Inactive</a>
                        @endif
                    </td>
                    @endif
                    <td>
                        @if(!$category->created_at == NULL)
                        {{$category->created_at->format('M-d-Y')}}
                        @else
                        <?php echo 'Null'; ?>
                        @endif
                    </td>
                    <td><a href="{{Route('category.edit',$category->id)}}" class="btn btn-xs btn-info">Edit</a>
                        <a id="delete" href="{{Route('category.delete',$category->id)}}" class="btn btn-xs btn-danger">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
                <!-- end of table body -->
            </table>
        </div>
        <!-- leftbody : categorytable end -->

    </div>
</div>
</div>
</section>



@endsection
