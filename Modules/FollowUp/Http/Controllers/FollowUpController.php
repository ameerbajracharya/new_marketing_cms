<?php

namespace Modules\FollowUp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseController;
use Modules\Inquiry\Entities\Inquiry;
use Modules\FollowUp\Entities\FollowUp as Followup;
use Session;
use Validator;
use DB;
use Auth;
use Modules\Followup\Http\Requests\AddFollowupValidation;


class FollowUpController extends BaseController
{

    protected $view_path = 'followup::';
    protected $base_route = 'followup.view';
    protected $databaseimage = '';
    protected $folder = '';
    protected $panel = 'FollowUp';
    protected $folder_path;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('followup::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        $data['inquiry'] = Inquiry::find($id);
        if(isset($data['inquiry'])){
            return view(parent::commondata($this->view_path.'create_followup'), compact('data'));
        }
        else
        {
            return redirect()->route('followup.show');
        }
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AddFollowupValidation $request, $id)
    {
        $followup = new FollowUp;
        $data['inquiry'] = Inquiry::find($id);
        $data['inquiry']->status = 1;
        $followup->user_id =  Auth::user()->id;
        $followup->ind = $data['inquiry']->id;
        $followup->icompany = $data['inquiry']->company;
        $followup->date = $request->date;
        $followup->followup = $request->followup;
        $followup->nextdate = $request->nextdate;
        $data['inquiry']->save();
        $followup-> save();
        Session::flash('success','Follow Up Stored Successfully');
        return redirect()->route('followup.view');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function view()
    {
        if(Auth::user()->name == 'admin'){
         $data['followup'] = Followup::paginate(10);
         $data['inquiry'] =  Inquiry::pluck('company');
         return view(parent::commondata($this->view_path.'list_followup'),compact('data'));
     }
     else{
        $data['followup'] = Followup::where('user_id', Auth::user()->id )->paginate(10);
        $data['inquiry'] =  Inquiry::pluck('company');
        return view(parent::commondata($this->view_path.'list_followup'),compact('data'));
    }
}

public function show()
{
    if(Auth::user()->name == 'admin'){
        $data['inquiry'] = Inquiry::paginate(8);
        $data['followup'] = Followup::paginate(8);
        return view(parent::commondata($this->view_path.'show_followup_admin'),compact('data'));
    }

    else{
        $data['inquiry'] = Inquiry::where('user_id', Auth::user()->id )->paginate(8);
        $data['followup'] = Followup::where('user_id', Auth::user()->id )->paginate(8);
        return view(parent::commondata($this->view_path.'show_followup'),compact('data'));  
    }
}

public function edit($id) 
{
   $data['inquiry'] = Inquiry::all();
   $data['followup'] = Followup::find($id);
   if(isset($data['followup'])){
    return view(parent::commondata($this->view_path.'edit_followup'),compact('data'));
}
else
    return redirect()->route('followup.show');
}

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(AddFollowupValidation $request, $id)
    {
        $data = Followup::find($id);
        $data->update($request->all());
        Session::flash('success','Follow Up Updated Successfully.');
        return redirect()->Route($this->base_route);    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
      $data = Followup::find($id);
      $data->delete();
      Session::flash('success','Follow Up Deleted Successfully.');
      return redirect()->Route($this->base_route);    }
  }
