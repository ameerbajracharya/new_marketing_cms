<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth','web'],'prefix' => 'dashboard/followup','as'=>'followup.'], function()
{	
	Route::get('/create/{id}', 	 		['uses'=>'FollowUpController@create',		'as'=>'create']);
	Route::post('/store/{id}',  	 	['uses'=>'FollowUpController@store',		'as'=>'store']);

	Route::get('/view',  				['uses'=>'FollowUpController@view',			'as'=>'view']);
	Route::get('/company/show',  		['uses'=>'FollowUpController@show',			'as'=>'show']);

	Route::get('/edit/{id}', 	   		['uses'=>'FollowUpController@edit',			'as'=>'edit']);
	Route::post('/update/{id}',  		['uses'=>'FollowUpController@update',		'as'=>'update']);

	Route::get('/delete/{id}',  		['uses'=>'FollowUpController@delete',		'as'=>'delete']);




});

