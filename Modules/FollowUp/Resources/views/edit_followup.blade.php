@extends('dashboard::layouts.master')

@section('title')


Edit FollowUp


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-6">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | Edit FollowUp</li>
            </div>
            <div class="col-md-6">
              <li class="text-right"><a href="{{Route('followup.create',$data['followup']->id)}}"><i class="fa fa-plus"></i> Create</a></li>
            </div>

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    

    {{-- form started --}}

    <form method="post" action="{{Route('followup.update', $data['followup']->id)}}" enctype="multipart/form-data">
      @csrf

      <div class="row">
        <!-- Slide -->
        <div class="col-lg-12">
          <!-- Slide Content -->
          <div class="form-wrapper well">
            <h2 class="text-center"><strong>FollowUp For {{$data['followup']->icompany}}</strong></h2><br>
            <div class="form-group">
             <label class="col-sm-3 control-label">Contacted Date:</label>
             <input type="date" class="form-control" placeholder="2018-07-22" name="date" value="{{$data['followup']->date}}" style="width:50%;">
             @if($errors->has('date'))
             <span class="text-danger">
              *{{$errors->first('date')}}
            </span>
            @endif
            <br>
          </div>


          <div class="form-group">
            <label class="col-sm-3 control-label">Followup Remark:</label>
            <textarea class="form-control" cols="5" rows="5" style="width:50%;" name="followup">{{$data['followup']->followup}}</textarea> 
            @if($errors->has('followup'))
            <span class="text-danger">
              *{{$errors->first('followup')}}
            </span>
            @endif
            <br>
          </div>

          <div class="form-group">
           <label class="col-sm-3 control-label">Next Followup Date:</label>
           <input type="date" class="form-control" placeholder="2018-07-22" name="nextdate" value="{{$data['followup']->nextdate}}" style="width:50%;">
           @if($errors->has('nextdate'))
           <span class="text-danger">
            *{{$errors->first('nextdate')}}
          </span>
          @endif
          <br>
        </div>

        <br>
        <!-- End of Keywords -->

        <button id="my-selector" class="btn btn-primary"><b>Save</b></button>

      </div>
    </div>
  </div>






</form>
{{-- form end --}}


</div>
</section>
@endsection
