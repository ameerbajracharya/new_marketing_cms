@extends('dashboard::layouts.master')

@section('title')

Company List


@endsection

@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | Company List</li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><a href="{{route('inquiry.create')}}"><i class="fa fa-plus"></i>Add Company</a></li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><a href="{{route('inquiry.view')}}"><i class="fa fa-eye"></i>View Companys</a></li>
                        </div>
                        <div class="col-md-2">
                            <li class="text-right"><a href="{{route('followup.view')}}"><i class="fa fa-eye"></i>View Follow Ups</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of user table -->
        <div class="row">
            <div class="col-lg-12">
                <!--left body: usertable -->
                <div class="table-responsive">
                    <table class="table">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Category</th>
                                <th>Company</th>
                                <th>Address</th>
                                <th>Phone</th>                        
                                <th>Action</th>

                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                         @foreach ($data['inquiry'] as $inquiry)
                         <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$inquiry->category}}</td>
                            <td>{{$inquiry->company}}</td>
                            <td>{{$inquiry->address}}</td>
                            <td>{{$inquiry->phoneno}}</td>              
                            <td>
                                @if($inquiry->status == 0)
                                <a class="btn btn-xs btn-info" href="{{Route('followup.create',$inquiry->id)}}">
                                Follow Up</a>
                                @else
                                 <a class="btn btn-xs btn-success" href="{{Route('followup.view',$inquiry->id)}}">
                                View/Edit</a>
                                @endif
                            </td>
                        </tr>
                        
                        @endforeach

                    </tbody>
                    <!-- end of table body -->
                </table>
            </div>
            <!-- bottom pagination -->
            <center>
                <div class="col-lg-12">
                   {!! $data['inquiry']->render() !!}
               </div>
           </center>
           <!-- end of bottom pagination -->
           <!-- leftbody : usertable end -->
       </div>
   </div>

   <!-- bottom pagination -->

   <!-- end of bottom pagination -->

</div>
</section>


@endsection
