@extends('dashboard::layouts.master')

@section('title')


FollowUp List


@endsection

@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | Follow Up List</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><a href="{{Route('followup.show')}}"><i class="fa fa-eye"></i>Company List</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of user table -->
        <div class="row">
            <div class="col-lg-12">
                <!--left body: usertable -->
                <div class="table-responsive">
                    <table class="table">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Company</th>
                                <th>Date</th>
                                <th>Follow Up</th>
                                <th>Next Date</th>
                                <th>Created Date</th>
                                <th colspan="2" style="text-align: center;">Action</th>

                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                         @foreach ($data['followup'] as $followup)
                         <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$followup->icompany}}</td>
                            <td>{{$followup->date}}</td>
                            <td>{{$followup->followup}}</td>
                            <td>{{$followup->nextdate}}</td>
                            <td>{{$followup->created_at->format('Y-M-d')}}</td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-info" href="{{Route('followup.edit',$followup->id)}}">
                                Edit</a>
                            </td>
                            @can('isAdmin')
                            <td>
                                <a id="delete" class="btn btn-xs btn-danger" href="{{Route('followup.delete',$followup->id)}}">Delete</a>
                            </td>
                            @endcan
                        </tr>

                        @endforeach

                    </tbody>
                    <!-- end of table body -->
                </table>
            </div>
            <!-- bottom pagination -->
            <center>
                <div class="col-lg-12">
                   {!! $data['followup']->render() !!}
               </div>
           </center>
           <!-- end of bottom pagination -->
           <!-- leftbody : usertable end -->
       </div>
   </div>

   <!-- bottom pagination -->

   <!-- end of bottom pagination -->

</div>
</section>


@endsection
