<?php

namespace Modules\FollowUp\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inquiry\Entities\Inquiry;

class FollowUp extends Model
{
    protected $fillable = ['user_id','icompany','date','followup','nextdate'];

    public function inquiry()
    {
    	return $this->belongsTo(Inquiry::class);
    }
     public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
