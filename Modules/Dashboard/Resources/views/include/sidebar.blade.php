<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">

        <span class="brand-text font-weight-light">Kite Cms</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('public/backend/dist/img/avatar.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('user.show',Auth::user()->id)}}" class="d-block">{{Auth::user()->name}}/{{Auth::user()->user_type}}</a>

            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav  nav-pills nav-sidebar flex-column navbar-nav" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->

                   <!-- User's Block -->
                   @can('isAdmin')
                   <li class="nav-item dropdown active">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                        <p>User
                            <i class="right fa fa-angle-down"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{Route('user')}}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{Route('user.create')}}" class="nav-link">
                                <i class="fa fa-plus nav-icon"></i>
                                <p>Register</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                <!-- Company Create -->
                <li class="nav-item">
                    <a class="nav-link" href="{{route('inquiry.create')}}">
                     <i class="fa fa-plus-square-o"></i>
                     <span>Company Create</span>
                 </a>
             </li>

             <!-- Company Inquiry -->
             <li class="nav-item ">
                <a class="nav-link" href="{{route('followup.show')}}">
                    <i class="fa fa-bullhorn"></i>
                    <span>Company Inquiry</span>
                </a>
            </li>

            <!-- Client -->
            <li class="nav-item ">
                <a class="nav-link" href="{{route('client')}}">
                 <i class="fa fa-users"></i>
                 <span>Client</span>
             </a>
         </li>

         <!-- Category -->
         <!-- <li class="nav-item ">
            <a class="nav-link" href="{{route('category')}}">
                <i class="fa fa-folder"></i>

                <span>Category</span>
            </a>
        </li>  -->

        <!-- Task -->
         <li class="nav-item ">
            <a class="nav-link" href="{{route('task')}}">
                <i class="fa fa-tasks"></i>

                <span>Task</span>
            </a>
        </li>
        

    </ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
