<?php

namespace Modules\Task\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Task\Entities\Staff;
use Modules\Task\Entities\Task;

class TaskController extends BaseController
{
    protected $view_path = 'task::';
    protected $base_route = 'task';
    protected $databaseimage = '';
    protected $folder = '';
    protected $panel = 'Task';
    protected $folder_path;

    public function __construct(Task $model){
        $this->model = $model;
        // $this->folderpath = public_path('images'. DIRECTORY_SEPARATOR).$this->folder;
    }

    public function index()
    {
        $data['task'] =Task::paginate(10);
        return view(parent::commondata($this->view_path.'index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['staff'] =Staff::all();
        return view(parent::commondata($this->view_path.'addtask'),compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {


//        foreach ($request->staff as $key => $staff){
//            dd($request->get('staff')[$key]);
//            $task=Task::create([
//                'name' => $request->name,
//                'description' => $request->description,
//                'deadline' => $request->deadline,
////                'staff' => $request->get('staff')[$key],
//            ]);
//
//        }
        $task = $this->model->create($request->all());
        $task->staff()->attach($request['staff']);
        Session::flash('success','Task Created successfully');
        return redirect()->route($this->base_route);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['task'] =Task::find($id);
        $this->rowExist($data['task']);
        $data['staff'] = Staff::all();
        return view(parent::commondata($this->view_path.'edit'),compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data['task'] = Task::find($id);
        $data['task']->update($request->all());
        Session::flash('success','Task update successfully');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function status($id){
        $this->statuschange($id);
        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        $data['task'] = Task::find($id);
        $this->rowExist($data['task']);
        $data['task']->delete();
        Session::flash('warning','Task deleted Successfully');
        return redirect()->Route($this->base_route);
    }
}
