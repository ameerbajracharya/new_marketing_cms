@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
@include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12">
                <!-- Title Bar -->
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{Route('task.create')}}">Add
                            Category</a></li>
                        </div>

                    </div>
                </ol>


                <!-- End of Title Bar -->
            </div>


        </div>
        {{-- overviewend --}}

        {{-- form started --}}
        <form method="post" action="{{Route('task.store')}}" enctype="multipart/form-data"
        onsubmit="return checkForm(this);">
        @csrf
        <div class="row">


            <div class="col-lg-6">
                <div class="form-wrapper well">
                    <div class="form-group">

                        <!-- name -->
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">Name:</label>
                            </div>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Enter Company's name"
                                name="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                <span class="text-danger">
                                    *{{$errors->first('name')}}
                                </span>
                                @endif
                            </div>

                        </div>
                        <!-- end of name -->
                        <br>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">Description:</label>
                            </div>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Address" name="description"
                                value="{{old('address')}}">
                                @if($errors->has('address'))
                                <span class="text-danger">
                                    *{{$errors->first('address')}}
                                </span>
                                @endif
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-wrapper well">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">Staff:</label>
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control select2" multiple="multiple" name="staff[]"  data-placeholder="Select Staffs">

                                 @foreach($data['staff'] as $staff)

                                 <option value="{{ $staff->id }}">

                                    {{ $staff->staff }}</option>

                                    @endforeach

                                </select>
                            </div>
                            @if($errors->has('staff'))
                            <span class="text-danger">
                                *{{$errors->first('staff')}}
                            </span>
                            @endif
                            <br>
                        </div>
                        <br>

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm">
                                <label class="col-lg-4 control-label">Deadline Date:</label>
                            </div>

                            <div class="col-sm-8">
                                <input type="date" class="form-control col-lg-8" placeholder="2018-07-22"
                                name="deadline" value="{{old('contractdate')}}">
                                @if($errors->has('contractdate'))
                                <span class="text-danger">
                                  *{{$errors->first('contractdate')}}
                              </span>
                              @endif
                          </div>
                          <br>

                      </div>
                      <br>

                  </div>
              </div>
          </div>

      </div>
  </div>

  <button type="submit" name="myButton" class="btn btn-primary"><b>Save</b></button>
</form>
{{-- form end --}}
</div>

</section>
<script>
    $(function() {
        var i =1;
        $("#addMore").click(function(e) {
            i++;
            e.preventDefault();
            $("#fieldList").append("<div id='staff"+i+"'> &nbsp;"+

                {{--"<input type=\"text\" name=\"staff[]\" class=\"form-control\"  placeholder=\"Staff\">" +--}}
                {{--"@if($errors->has('staff'))\n" +--}}
                {{--"                        <span class=\"text-danger\">*{{$errors->first('staff')}}</span>\n" +--}}
                {{--"                        <br>\n" +--}}
                {{--"                        @endif\n" +--}}
                {{--"\n" +--}}
                "<select name=\"staff\" class=\"form-control\">" +
                "<option>  Select a Staff </option>\n"+
                "@foreach($data['staff'] as $staff)\n"+
                "@if($staff->status == 1)\n"+
                "<option>  {{$staff->name}}</option>\n"+
                "@endif\n"+
                "@endforeach\n"+
                "</select>\n" +

                "                                </div>");
        });
        $("#removeLast").click(function(e) {
            e.preventDefault();
            $("#staff"+i+"").remove();
            i--;
        });


    });
</script>
@endsection
{{--"<select name=\"category\" class=\"form-control\">" +--}}
    {{--    "<option>  Select a Staff </option>\n"+--}}
    {{--    "@foreach($data['staff'] as $staff)\n"+--}}
    {{--        "<option>  {{$staff->staff}}</option>\n"+--}}
    {{--    "@endforeach\n"+--}}
{{--"</select>\n"--}}

