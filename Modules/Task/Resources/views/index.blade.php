@extends('dashboard::layouts.master')

@section('content')
  @include('dashboard::include.header')
<section class="content">
  <div class="container-fluid">
    <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <div class="row">
                            <div class="col-md-6">
                                <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}} </li>
                            </div>
                            <div class="col-md-6">
                                <li class="text-right"><i class="fa fa-plus"></i><a href="{{route('task.create')}}">Add</a></li>
                            </div>

                        </div>
                    </ol>
                </div>
            </div>
            <!-- end of overstart -->

            <!-- main content -->
            <!-- start of client table -->
            <div class="row">
                <div class="col-xs-12">
                    <!--left body: clienttable -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <!--start heading of the table  -->
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>staff</th>
                                <th>Deadline</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th colspan="3" style="text-align: center;">Action</th>
                            </tr>
                            </thead>
                            <!-- end of table heading -->
                            <!-- table body start -->
                            <tbody>
                            @foreach($data['task'] as $task)
                                <tr>
                                    <td>{{$task->name}}</td>
                                    <td>{{$task->description}}</td>

                                    <td>
                                        {{$task->staff}}
                                    </td>
                                    <td>{{$task->deadline}}</td>

                                    @if($task->status == 0)
                                        <td>
                                            @if($task->status == 0)
                                                <a href="{{route('task.status',$task->id)}}" class="btn btn-xs  btn-danger">Inactive</a>
                                            @else
                                                <a href="" class="btn btn-xs btn-info">Active</a>
                                            @endif
                                        </td>
                                    @else
                                        <td>
                                            @if($task->status == 1)
                                                <a href="{{route('task.status',$task->id)}}" class="btn btn-xs  btn-info">Active</a>

                                            @else
                                                <a href="" class="btn btn-xs btn-danger">Inactive</a>
                                            @endif
                                        </td>
                                    @endif
                                    <td>
                                        @if(!$task->created_at == NULL)
                                            {{$task->created_at->format('M-d-Y')}}
                                        @else
                                            <?php echo 'Null'; ?>
                                        @endif
                                    </td>
                                    <td><a href="{{Route('task.edit',$task->id)}}" class="btn btn-xs btn-info">Edit</a>
                                    </td>
                                    <td><a id="delete" href="{{Route('task.delete',$task->id)}}" class="btn btn-xs btn-danger">Delete</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                            <!-- end of table body -->
                        </table>
                    </div>
                    <!-- bottom pagination -->
                    <div class="col-lg-4">
                        <center>
                            {!! $data['task']->render() !!}
                        </center>
                    </div>
                    <!-- end of bottom pagination -->
                    <!-- leftbody : tasktable end -->
                </div>
            </div>

        </div>
    </section>


@endsection

