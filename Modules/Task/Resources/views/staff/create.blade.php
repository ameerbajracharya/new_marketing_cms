@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
<section id="main-content">
    <section class="wrapper">
        {{-- overview --}}
        <div class="row">
            <div class="col-lg-12">
                <!-- Title Bar -->
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('staff')}}">View</a>
                            </li>
                        </div>

                    </div>
                </ol>


                <!-- End of Title Bar -->
            </div>


        </div>
        {{-- overviewend --}}

        {{-- form started --}}
        <form method="post" action="{{Route('staff.store')}}" enctype="multipart/form-data"
              onsubmit="return checkForm(this);">
            @csrf
            <div class="row">


                <div class="col-lg-6">
                    <div class="form-wrapper well">
                        <div class="form-group">

                            <!-- name -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">Name:</label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="Enter Staff's name"
                                           name="name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                    <span class="text-danger">
                    *{{$errors->first('name')}}
                  </span>
                                    @endif
                                </div>

                            </div>
                            <!-- end of name -->
                            <br>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label">Description:</label>
                                </div>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="Address" name="description"
                                           value="{{old('address')}}">
                                    @if($errors->has('address'))
                                    <span class="text-danger">
                                                    *{{$errors->first('address')}}
                                          </span>
                                    @endif
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

            </div>
            </div>

            <button type="submit" name="myButton" class="btn btn-primary"><b>Save</b></button>
        </form>
        {{-- form end --}}


    </section>

</section>

@endsection

