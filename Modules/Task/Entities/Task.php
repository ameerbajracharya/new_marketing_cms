<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name','description','staff','deadline','status'];
    public function staff()
    {
        return $this->belongsToMany(Staff::class);
    }
}
